import React, { Component } from 'react'
import { dataShoe } from './data_shoe'
import Shoe_Cart from './Shoe_Cart'
import Shoe_Detail from './Shoe_Detail'
import Shoe_List from './Shoe_List'



export default class Shoe_Shop_Redux extends Component {


    render() {
        return (
            <div>
                <Shoe_Cart />
                <Shoe_List />
                <Shoe_Detail />
            </div>
        )
    }
}
