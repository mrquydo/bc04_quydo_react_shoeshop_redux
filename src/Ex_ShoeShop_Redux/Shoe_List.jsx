import React, { Component } from 'react'
import { connect } from 'react-redux'
import Shoe_Item from './Shoe_Item'

class Shoe_List extends Component {
    render() {
        return (
            <div className='container'>
                <div className="row">
                    {this.props.data.map((item, index) => {
                        return (
                            <div key={index} className="col-3">
                                <Shoe_Item
                                    item={item} />
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        data: state.shoeReducer.shoeList
    }
}

export default connect(mapStateToProps)(Shoe_List)


