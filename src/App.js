import logo from "./logo.svg";
import "./App.css";
import Shoe_Shop_Redux from "./Ex_ShoeShop_Redux/Shoe_Shop_Redux";

function App() {
  return (
    <div className="App">
      <Shoe_Shop_Redux />
    </div>
  );
}

export default App;
